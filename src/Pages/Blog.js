import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import blog1 from '../images/blog/blog1.jpg'
import Navbar from '../Components/Navbar'
import Footer from '../Components/Footer'


class Blog extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <section id="blog" className="p-5">
                    <div className="container">
                        <div class="text-center">
                            <h2>Blogs</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                        </div>
                        <div className="blog">
                            <div className="row">
                                <div class="blog-item">
                                    <div className="row">
                                        <div className="col-md-6 blog-content">
                                            <Link to="#"><img className="img-responsive img-blog" src={blog1} width="100%" alt="" /></Link>
                                            <h2><Link to="blog-item.html">Consequat bibendum quam liquam viverra</Link></h2>
                                            <p>Curabitur quis libero leo, pharetra mattis eros. Praesent consequat libero eget dolor convallis vel rhoncus magna scelerisque. Donec nisl ante, elementum eget posuere a, consectetur a metus. Proin a adipiscing sapien. Suspendisse vehicula porta lectus vel semper. Nullam sapien elit, lacinia eu tristique non.posuere at mi. Morbi at turpis id urna ullamcorper ullamcorper.</p>
                                            <Link className="btn btn-primary readmore" to="blog-item.html">Read More <i className="fa fa-angle-right"></i></Link>
                                        </div>

                                        <div className="col-md-6 blog-content">
                                            <Link to="#"><img className="img-responsive img-blog" src={blog1} width="100%" alt="" /></Link>
                                            <h2><Link to="blog-item.html">Consequat bibendum quam liquam viverra</Link></h2>
                                            <p>Curabitur quis libero leo, pharetra mattis eros. Praesent consequat libero eget dolor convallis vel rhoncus magna scelerisque. Donec nisl ante, elementum eget posuere a, consectetur a metus. Proin a adipiscing sapien. Suspendisse vehicula porta lectus vel semper. Nullam sapien elit, lacinia eu tristique non.posuere at mi. Morbi at turpis id urna ullamcorper ullamcorper.</p>
                                            <Link className="btn btn-primary readmore" to="blog-item.html">Read More <i className="fa fa-angle-right"></i></Link>
                                        </div>
                                        
                                    </div>
                                </div> {/*.blog-item */}
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}

export default Blog