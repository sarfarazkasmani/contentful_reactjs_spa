import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';
import services1 from '../images/services/services1.png'
import services2 from '../images/services/services2.png'
import services3 from '../images/services/services3.png'
import services4 from '../images/services/services4.png'
import services5 from '../images/services/services5.png'
import services6 from '../images/services/services6.png'

class Services extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <section id="services" className="service-item p-5">
                    <div className="container">
                        <div class="text-center">
                            <h2>Our Service</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br /> et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="media services-wrap">
                                    <div class="float-left">
                                        <img class="img-responsive pr-2" src={services1} />
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">SEO Marketing</h3>
                                        <p>Lorem ipsum dolor sit ame consectetur adipisicing elit</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="media services-wrap">
                                    <div class="float-left">
                                        <img class="img-responsive pr-2" src={services2} />
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">SEO Marketing</h3>
                                        <p>Lorem ipsum dolor sit ame consectetur adipisicing elit</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="media services-wrap">
                                    <div class="float-left">
                                        <img class="img-responsive pr-2" src={services3} />
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">SEO Marketing</h3>
                                        <p>Lorem ipsum dolor sit ame consectetur adipisicing elit</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="media services-wrap">
                                    <div class="float-left">
                                        <img class="img-responsive pr-2" src={services4} />
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">SEO Marketing</h3>
                                        <p>Lorem ipsum dolor sit ame consectetur adipisicing elit</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="media services-wrap">
                                    <div class="float-left">
                                        <img class="img-responsive pr-2" src={services5}/>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">SEO Marketing</h3>
                                        <p>Lorem ipsum dolor sit ame consectetur adipisicing elit</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="media services-wrap">
                                    <div class="float-left">
                                        <img class="img-responsive pr-2" src={services6} />
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">SEO Marketing</h3>
                                        <p>Lorem ipsum dolor sit ame consectetur adipisicing elit</p>
                                    </div>
                                </div>
                            </div>
                        </div>{/*!.row */}
                    </div>{/*!.container */}
                </section>
                <Footer />
            </div >
        );
    }
}

export default Services