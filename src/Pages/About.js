import React, {Component} from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';


class About extends Component {
    render() {
        return(
            <div>
                <Navbar />
                <div className="container">
                    <h2>About</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis rhoncus sollicitudin fermentum. Nulla interdum velit eu eros pretium, a venenatis est maximus. Nam leo dui, suscipit sed sapien nec, faucibus varius tortor. Vestibulum interdum quis diam sed scelerisque. Aenean facilisis rhoncus lorem vel varius.</p>
                </div>
                <Footer />
            </div>
        );
    }
}

export default About