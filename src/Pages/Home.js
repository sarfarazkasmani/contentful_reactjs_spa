import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';
import silder1 from '../images/slider_one.jpg';
import './main.css';
import Gallery from 'react-grid-gallery';

const IMAGES =
    [{
        src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
        thumbnail: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212,

        caption: "After Rain (Jeshu John - designerspics.com)"
    },
    {
        src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
        thumbnail: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212,
        tags: [{ value: "Ocean", title: "Ocean" }, { value: "People", title: "People" }],
        caption: "Boats (Jeshu John - designerspics.com)"
    },

    {
        src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
        thumbnail: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212
    },
    {
        src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
        thumbnail: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212
    },
    {
        src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
        thumbnail: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212
    },
    {
        src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
        thumbnail: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212
    }]

class Home extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <div className="">
                    {/* Carousel start here */}
                    <div id="carouselExampleControls" className="container carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img className="d-block w-100" src={silder1} alt="First slide" />
                            </div>
                            <div className="carousel-item">
                                <img className="d-block w-100" src={silder1} alt="Second slide" />
                            </div>
                            <div className="carousel-item">
                                <img className="d-block w-100" src={silder1} alt="Third slide" />
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                    {/* Carousel ends here */}
                    <section id="feature" >
                        <div className="container text-center pt-4">
                            <h2>Features</h2>
                            <p className="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br /> et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>

                        <div className="container">
                            <div className="row">
                                <div className="col-md-4">
                                    <div class="feature-wrap">
                                        <i class="fa fa-laptop"></i>
                                        <h2>Fresh and Clean</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div class="feature-wrap">
                                        <i class="fa fa-comments"></i>
                                        <h2>Retina ready</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div class="feature-wrap">
                                        <i class="fa fa-download"></i>
                                        <h2>Easy to customize</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div class="feature-wrap">
                                        <i class="fa fa-leaf"></i>
                                        <h2>Adipisicing elit</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div class="feature-wrap">
                                        <i class="fa fa-cogs"></i>
                                        <h2>Sed do eiusmod</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div class="feature-wrap">
                                        <i class="fa fa-heart"></i>
                                        <h2>Labore et dolore</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="portfolio" >
                        <div className="container text-center pt-4">
                            <h2>Portfolios</h2>
                            <p className="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br /> et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                        <div className="container">
                            <Gallery images={IMAGES} />
                            <div className="clearfix"></div>
                        </div>
                    </section>

                    <section id="faqs">
                        <div className="container text-center pt-4">
                            <h2>FAQ's</h2>
                            <p className="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br /> et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                        <div className="container">

                            <div className="col-md-12">

                                
                                    <div className="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingOne3">
                                                <h4 className="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne3">
                                                        Section 1
                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne3">
                                                <div className="panel-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingTwo3">
                                                <h4 className="panel-title">
                                                    <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwo3" aria-expanded="false" aria-controls="collapseTwo3">
                                                        Section 2
                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo3">
                                                <div class="panel-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingThree3">
                                                <h4 className="panel-title">
                                                    <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                                        Section 3
                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree3">
                                                <div className="panel-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>

                        </div>

                    </section>

                </div>

                <div className="spacer"></div>
                <Footer />
            </div>
        );
    }
}

export default Home