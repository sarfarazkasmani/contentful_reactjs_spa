import React, { Component } from 'react'
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';


class Contact extends Component {

    state = {
        name: "sarfaraz",
    }

    changeName = (event) => {
        this.setState({
            name: event.target.value,
        })
    }

    render() {
        return (
            <div>
                <Navbar />
                <section id="contact-page" className="p-5">
                    <div className="container">
                        <div className="text-center">
                            <h2>Drop Your Message</h2>
                            <p className="lead">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>


                        {/* <div className="status alert alert-success"></div> */}
                        <form id="main-contact-form" className="contact-form" name="contact-form" method="post" action="">
                            <div className="row contact-wrap">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Name *</label>
                                        <input type="text" name="name" className="form-control" onChange={this.changeName} value={this.state.name} required="required" />
                                        <div>{this.state.name}</div>
                                    </div>
                                    <div className="form-group">
                                        <label>Email *</label>
                                        <input type="email" name="email" className="form-control" required="required" />
                                    </div>
                                    <div className="form-group">
                                        <label>Phone</label>
                                        <input type="number" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label>Company Name</label>
                                        <input type="text" className="form-control" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Subject *</label>
                                        <input type="text" name="subject" className="form-control" required="required" />
                                    </div>
                                    <div className="form-group">
                                        <label>Message *</label>
                                        <textarea name="message" id="message" required="required" className="form-control" rows="8"></textarea>
                                    </div>
                                    <div className="form-group">
                                        <button type="submit" name="submit" className="btn btn-primary btn-lg" required="required" >Submit Message</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </section>
                <Footer />
            </div>

        );
    }
}

export default Contact