import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import Home from '../src/Pages/Home'
import About from '../src/Pages/About'
import Services from '../src/Pages/Services'
import Blog from '../src/Pages/Blog'
import Contact from '../src/Pages/Contact'

class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <Route exact path="/" component = {Home} />
            <Route exact path="/About" component = {About} />
            <Route exact path="/Services" component = {Services} />
            <Route exact path="/Blog" component = {Blog} />
            <Route exact path="/Contact" component = {Contact} />
          </div>
        </Router>
    );
  }
}

export default App;
